﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment
{
    public class Payment
    {
        public string ReferenceID { get; set; }
        public string Client { get; set; }
        public int PlatformID { get; set; }
        public int BankID { get; set; }
        public int TypeID { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public string SubmitDateTime { get; set; }
        public string RedirectUrl { get; set; }
        public string NotificationUrl { get; set; }
        public string IP { get; set; }
        public string Remark { get; set; }
        public string PlayerID { get; set; }

        public string Sign { get; set; }
    }
}
