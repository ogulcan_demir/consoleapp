﻿using System;
using System.Data.SqlClient;

namespace Payment
{
    public class Program
    {
        static void Main(string[] args)
            {
                Console.WriteLine("1.Read");
                Console.WriteLine("2.Add");
                Console.WriteLine("Your Choice:");
                string choose = Console.ReadLine();
                if (choose == "1")
                {
                    Read();
                }
                else if (choose == "2")
                {
                    Add();
                    Read();
                }


                Console.ReadLine();
            }

            static SqlConnection baglanti;
            static SqlCommand komut;
            static SqlDataReader reader;

            public static void Read()
            {
                baglanti = new SqlConnection();
                baglanti.ConnectionString = @"server=(localdb)\mssqllocaldb;initial catalog=Paymentdb;integrated security=true";
                baglanti.Open();
                komut = new SqlCommand();
                komut.Connection = baglanti;
                komut.CommandText = ("Select * from Payment");
                
                reader = komut.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("Order Number :"+ reader[0]);
                    Console.WriteLine("Client ID :" + reader[1]);
                    Console.WriteLine("Platform ID :" + reader[2]);
                    Console.WriteLine("Bank ID :" + reader[3]);
                    Console.WriteLine("Type ID :" + reader[4]);
                    Console.WriteLine("Currency :" + reader[5]);
                    Console.WriteLine("Amount :" + reader[6]);
                    Console.WriteLine("Submitted Time :" + reader[7]);
                    Console.WriteLine("Redirect Url :" + reader[8]);
                    Console.WriteLine("Notification Url :" + reader[9]);
                    Console.WriteLine("IP :" + reader[10]);
                    Console.WriteLine("Remark :" + reader[11]);
                    Console.WriteLine("Player ID :" + reader[12]);
                    Console.WriteLine("Signature :" + reader[13]);



                }
                baglanti.Close();

                Console.WriteLine("Okay!");
            }

            public static void Add()
            {
                Console.WriteLine("ReferenceID:");
                string ReferenceID = Console.ReadLine();
                Console.WriteLine("Client:");
                string Client = Console.ReadLine();
                Console.WriteLine("PlatformID:");
                int PlatformID = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("BankID:");
                int BankID = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("TypeID:");
                int TypeID = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("CurrencyCode:");
                string CurrencyCode = Console.ReadLine();
                Console.WriteLine("Amount:");
                decimal Amount = Convert.ToDecimal(Console.ReadLine());
                Console.WriteLine("SubmitDateTime:");
                string SubmitDateTime = Console.ReadLine();
                Console.WriteLine("RedirectUrl:");
                string RedirectUrl = Console.ReadLine();
                Console.WriteLine("NotificationUrl:");
                string NotificationUrl = Console.ReadLine();
                Console.WriteLine("IP:");
                string IP = Console.ReadLine();
                Console.WriteLine("Remark:");
                string Remark = Console.ReadLine();
                Console.WriteLine("PlayerID:");
                string PlayerID = Console.ReadLine();
                Console.WriteLine("Sign:");
                string Sign = Console.ReadLine();

                baglanti = new SqlConnection();
                baglanti.ConnectionString = @"server=(localdb)\mssqllocaldb;initial catalog=Paymentdb;integrated security=true";
                baglanti.Open();
                komut = new SqlCommand();
                komut.Connection = baglanti;
                komut.CommandText = "INSERT INTO Payment(ReferenceID,Client,PlatformID,BankID,TypeID,CurrencyCode,Amount,SubmitDateTime,RedirectUrl,NotificationUrl,IP,Remark,PlayerID,Sign) VALUES (@ReferenceID,@Client,@PlatformID,@BankID,@TypeID,@CurrencyCode,@Amount,@SubmitDateTime,@RedirectUrl,@NotificationUrl,@IP,@Remark,@PlayerID,@Sign";
                
                baglanti.Close();
                
                
                Console.WriteLine("Added!");
                

            }
        
    }
}
